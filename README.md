# Node_API_CRUD

First approach to a NODE CRUD API.

## Routes
```
/movies
```
```
/movies/[id:number]
```
```
/movies?name=[whatever]
```

### Installing
Install all the modules

```
npm install
```

For dev run use:

```
npm run dev
```
 

## Authors

* **Paloma Sánchez** - *Initial work* - [@PalomaSanchez](https://gitlab.com/PalomaSanchez)

