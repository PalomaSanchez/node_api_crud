const express = require("express");
const fs = require("fs");

const PORT = 3000;
const server = express();

const router = express.Router();

router.get("/movies", (req, res) => {
  fs.readFile("movies.json", (err, data) => {
    if (err) {
      console.log("There was an error reading the file!");
    } else {
      // Ahora utilizamos req.query
      const name = req.query.name;
      const parsedData = JSON.parse(data);
      // Si no tenemos req.query.name enviaremos todas las películas
      // ya que no habrá búsqueda por parte del usuario
      if (!name) {
        res.json(parsedData);

      } else {
        // const hasMovie = parsedData.some(
        //   (item) => item.name.toLowerCase() === name
        // );
        const numero = parsedData.findIndex(
          (item) => item.name.toLowerCase() === name
        );

        if (numero < 0) {
          res.json(parsedData[numero]);
          //res.send(`${JSON.stringify(JSON.parse(data)[numero])}`);
        } else {
          // const another = Object.create(parsedData[0]);
          // another.id = parsedData.length + 1;
          // another.name = name;

          const another = {
            id:parsedData.length + 1,
             nombre:name
          };

          parsedData.push(another);

          fs.writeFile("movies.json", JSON.stringify(parsedData), () => {
            if (err) throw err;
            console.log("File saved!");
            res.json(parsedData);
          });
        }
      }
    }
  });
});

router.get("/movies/:id", (req, res) => {

  fs.readFile("movies.json", (err, data) => {

    if (err) {
      console.log("There was an error reading the file!");

    } else {
      // Ahora utilizamos req.query
      const id = req.params.id;
      const parsedData = JSON.parse(data);

      // Si no tenemos req.query.name enviaremos todas las películas
      // ya que no habrá búsqueda por parte del usuario
      if (!id) {
        res.json(parsedData);

      } else {
        const hasMovie = parsedData.some((item) => item.id == id);
        const numero = parsedData.findIndex((item) => item.id == id);

        if (hasMovie) {
          res.json(parsedData[numero]);
          //res.send(`${JSON.stringify(JSON.parse(data)[numero])}`);

        } else {
          res.send("We could not find the movie you are looking for!");
        }
      }
      res.end(data);
    }
  });
});

server.use("/", router);

server.listen(PORT, () => {

  console.log(`Server running in http://localhost:${PORT}`);
});
